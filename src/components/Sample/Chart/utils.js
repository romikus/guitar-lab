export const xToSec = (x, duration, width) =>
  x * duration / width

export const secToX = (sec, duration, width) =>
  sec * width / duration
